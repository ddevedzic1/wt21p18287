const Sequelize = require("sequelize");
const sequelize = new Sequelize("wt2118287", "root", "password", {
  host: "127.0.0.1",
  dialect: "mysql",
  logging: false,
});
const db = {};

db.Sequelize = Sequelize;
db.sequelize = sequelize;

//import modela
db.vjezba = require("./vjezba.js")(sequelize);
db.zadatak = require("./zadatak.js")(sequelize);
db.student = require("./student.js")(sequelize);
db.grupa = require("./grupa.js")(sequelize);

//relacije
// Veza 1-n vise studenata se moze nalaziti u grupi
db.grupa.hasMany(db.student, { as: "studentiGrupe" });

// Veza 1-n vise zadataka se moze nalaziti na vjezbi
db.vjezba.hasMany(db.zadatak, { as: "zadaciVjezbe" });

module.exports = db;

require("dotenv").config();

const express = require("express");
const path = require("path");
const csv = require("csv-parser");
const createCsvWriter = require("csv-writer").createObjectCsvWriter;
const fs = require("fs");
const bodyParser = require("body-parser");
const db = require("./db.js");
const grupa = require("./grupa.js");
const { resolve } = require("path");

app = express();

app.use(express.static(path.join(__dirname, "public")));
app.use(express.static(path.join(__dirname, "public/html")));
app.use(bodyParser.json());

///////////////////////////////////////

app.post("/student/", async (req, res) => {
  if (
    !(
      typeof req.body.ime === "string" &&
      typeof req.body.prezime === "string" &&
      typeof req.body.index === "string" &&
      typeof req.body.grupa === "string"
    )
  ) {
    return res.status(400).send({ status: "Neispravni podaci" });
  }

  try {
    var s = await db.student.findOne({
      where: { index: req.body.index },
    });
    if (s) {
      return res.status(400).send({
        status: "Student sa indexom " + req.body.index + " već postoji!",
      });
    }
  } catch (err) {
    return res.status(400).send({
      status: "Greska pri dohvatanju studenta",
    });
  }

  try {
    const [grupa, createdGrupa] = await db.grupa.findOrCreate({
      where: { naziv: req.body.grupa },
    });

    try {
      await db.student.create({
        ime: req.body.ime,
        prezime: req.body.prezime,
        index: req.body.index,
        GrupaId: grupa.id,
      });
      return res.status(200).send({ status: "Kreiran student!" });
    } catch (err) {
      return res.status(400).send({
        status: "Greska pri kreiranju studenta",
      });
    }
  } catch (err) {
    return res.status(400).send({
      status: "Greska pri dohvatanju/kreiranju grupe",
    });
  }
});

app.put("/student/:index", async (req, res) => {
  if (!(typeof req.body.grupa === "string"))
    return res.status(400).send({ status: "Neispravni podaci" });

  try {
    var student = await db.student.findOne({
      where: { index: req.params.index },
    });
    if (!student) {
      return res.status(400).send({
        status: "Student sa indexom " + req.params.index + " ne postoji",
      });
    }
  } catch (err) {
    return res.status(400).send({
      status: "Greska pri dohvatanju studenta",
    });
  }

  try {
    const [grupa, createdGrupa] = await db.grupa.findOrCreate({
      where: { naziv: req.body.grupa },
    });
    try {
      var rez = await db.student.update(
        { GrupaId: grupa.id },
        { where: { index: req.params.index } }
      );
      if (rez[0] === 1)
        return res
          .status(200)
          .send({ status: "Promjenjena grupa studentu " + req.params.index });
      else {
        return res.status(400).send({
          status: "Student sa indexom " + req.params.index + " ne postoji",
        });
      }
    } catch (err) {
      return res.status(400).send({
        status: "Greska pri azuriranju studenta",
      });
    }
  } catch (err) {
    return res.status(400).send({
      status: "Greska pri dohvatanju/kreiranju grupe",
    });
  }
});

app.get("/vjezbe/", async (req, res) => {
  var nazivi = [];
  var vjezbe = [];
  var zadaci = [];

  try {
    vjezbe = await db.vjezba.findAll();
  } catch (err) {
    return res.json();
  }

  try {
    zadaci = await db.zadatak.findAll();
  } catch (err) {
    return res.json();
  }

  for (let i = 0; i < vjezbe.length; i++) {
    let tempZadaci = zadaci.filter(
      (zadatak) => zadatak.dataValues.VjezbaId === vjezbe[i].dataValues.id
    );
    let naziviZadataka = [];
    for (let j = 0; j < tempZadaci.length; j++)
      naziviZadataka.push(tempZadaci[j].dataValues.naziv);
    nazivi.push({
      nazivVjezbe: vjezbe[i].dataValues.naziv,
      naziviZadataka: naziviZadataka,
    });
  }

  return res.json({
    nazivi: nazivi,
  });

  /*
  try {
    for (let i = 0; i < vjezbe.length; i++) {
      zadaci = await db.zadatak.findAll({
        where: { VjezbaId: vjezbe[i].dataValues.id },
      });
      brojZadatakaPoVjezbi.push(zadaci.length);
      let naziviZadataka = [];
      for (let j = 0; j < zadaci.length; j++) {
        console.log("ispis");
        console.log(zadaci[0].dataValues.naziv);
        naziviZadataka.push(zadaci[j].dataValues.naziv);
      }
      nazivi.push({
        nazivVjezbe: vjezbe[i].dataValues.naziv,
        naziviZadataka: naziviZadataka,
      });
    }
  } catch (err) {
    return res.json();
  }
  */
  /*
  brojVjezbi = vjezbe.length;

  if (brojVjezbi !== brojZadatakaPoVjezbi.length) return res.json();

  if (!(brojVjezbi > 0 && brojVjezbi < 15)) return res.json();

  for (let i = 0; i < brojZadatakaPoVjezbi.length; i++)
    if (!(brojZadatakaPoVjezbi[i] > -1 && brojZadatakaPoVjezbi[i] < 10))
      return res.json();*/

  /*
  console.log(brojVjezbi);
  console.log(brojZadatakaPoVjezbi);
  console.log(nazivi);*/

  /*
  let vjezbeData = [];

  let readable = fs.createReadStream(path.join(__dirname, "vjezbe.csv"));
  readable.on("error", (err) => {
    return res.json();
  });
  readable
    .pipe(csv({}))
    .on("data", (data) => vjezbeData.push(data))
    .on("end", () => {
      if (vjezbeData.length > 0) {
        if (
          typeof vjezbeData[0].brojVjezbi !== "undefined" &&
          typeof vjezbeData[0].brojZadataka !== "undefined"
        ) {
          if (
            vjezbeData[0].brojVjezbi > 0 &&
            vjezbeData[0].brojVjezbi < 15 &&
            Number.isInteger(eval(vjezbeData[0].brojVjezbi))
          ) {
            vjezbeData[0].brojVjezbi = eval(vjezbeData[0].brojVjezbi);
          } else {
            return res.json();
          }
          try {
            var brojZadataka = eval(vjezbeData[0].brojZadataka);
          } catch (err) {
            return res.json();
          }
          if (
            Object.prototype.toString.call(brojZadataka) === "[object Array]"
          ) {
            if (vjezbeData[0].brojVjezbi !== brojZadataka.length)
              return res.json();
            for (let i = 0; i < brojZadataka.length; i++) {
              if (
                !(
                  brojZadataka[i] > -1 &&
                  brojZadataka[i] < 10 &&
                  Number.isInteger(brojZadataka[i])
                )
              ) {
                return res.json();
              }
            }
            vjezbeData[0].brojZadataka = brojZadataka;
          } else {
            return res.json();
          }
          res.json({
            brojVjezbi: vjezbeData[0].brojVjezbi,
            brojZadataka: vjezbeData[0].brojZadataka,
          });
        } else {
          return res.json();
        }
      } else {
        return res.json();
      }
    });*/
});

app.post("/vjezbe", async (req, res) => {
  let vjezbeData = req.body;
  var greske = "";
  let brojVjezbiTacnost = true;
  if (
    !(
      Number.isInteger(vjezbeData.brojVjezbi) &&
      vjezbeData.brojVjezbi < 15 &&
      vjezbeData.brojVjezbi > 0
    )
  ) {
    greske += "Pogresan parametar brojVjezbi";
    brojVjezbiTacnost = false;
  }
  if (Array.isArray(vjezbeData.brojZadataka)) {
    for (let i = 0; i < vjezbeData.brojZadataka.length; i++) {
      if (
        !(
          Number.isInteger(vjezbeData.brojZadataka[i]) &&
          vjezbeData.brojZadataka[i] >= 0 &&
          vjezbeData.brojZadataka[i] < 10
        )
      ) {
        if (greske.length == 0) {
          greske += "Pogresan parametar z" + i;
        } else {
          greske += ",z" + i;
        }
      }
    }

    if (
      brojVjezbiTacnost &&
      vjezbeData.brojVjezbi != vjezbeData.brojZadataka.length
    ) {
      if (greske.length == 0) greske += "Pogresan parametar brojZadataka";
      else greske += ",brojZadataka";
    }
  } else {
    if (greske.length == 0) {
      greske += "Pogresan parametar brojZadataka";
    } else {
      greske += ",brojZadataka";
    }
  }
  if (greske.length == 0) {
    try {
      var stariBrojVjezbi = await db.vjezba.count();
    } catch (err) {
      return res.status(400).send({
        status: "error",
        data: "Greska pri komunikaciji s bazom podataka",
      });
    }
    try {
      var kreiraneVjezbe = [];
      for (let i = 0; i < vjezbeData.brojVjezbi; i++)
        kreiraneVjezbe.push(
          await db.vjezba.create({
            naziv: "Vjezba " + (stariBrojVjezbi + i + 1),
          })
        );
    } catch (err) {
      return res.status(400).send({
        status: "error",
        data: "Greska pri komunikaciji s bazom podataka",
      });
    }
    try {
      for (let i = 0; i < kreiraneVjezbe.length; i++)
        for (let j = 0; j < vjezbeData.brojZadataka[i]; j++)
          await db.zadatak.create({
            naziv: "Zadatak " + (j + 1),
            VjezbaId: kreiraneVjezbe[i].id,
          });
      return res.status(200).send(req.body);
    } catch (err) {
      return res.status(400).send({
        status: "error",
        data: "Greska pri komunikaciji s bazom podataka",
      });
    }
    /*
    const csvWriter = createCsvWriter({
      path: path.join(__dirname, "vjezbe.csv"),
      header: [
        { id: "brojVjezbi", title: "brojVjezbi" },
        { id: "brojZadataka", title: "brojZadataka" },
      ],
    });
    let dataToWrite = [];
    let brojZadatakaToWrite = "[";
    for (let i = 0; i < req.body.brojZadataka.length; i++)
      brojZadatakaToWrite += req.body.brojZadataka[i] + ",";
    brojZadatakaToWrite = brojZadatakaToWrite.slice(0, -1);
    brojZadatakaToWrite += "]";
    dataToWrite.push({
      brojVjezbi: "" + req.body.brojVjezbi,
      brojZadataka: brojZadatakaToWrite,
    });
    csvWriter
      .writeRecords(dataToWrite)
      .then(() => res.status(200).send(req.body));*/
  } else {
    return res.status(422).send({ status: "error", data: greske });
  }
});

///////////////////////////////////////

const PORT = process.env.PORT || 3000;

app.listen(PORT, () => {
  console.log(`Listening on Port: ${PORT}`);
});

function dohvatiPodatke() {
  VjezbeAjax.dohvatiPodatke((error, data) => {
    if (!error) {
      VjezbeAjax.iscrtajVjezbe(document.getElementById("odabirVjezbe"), data);
    } else {
      document.getElementById("odabirVjezbe").innerHTML =
        "Podaci nisu dostupni";
    }
  });
}
dohvatiPodatke();

function dodajStudenta() {
  let data = { ime: "", prezime: "", index: "", grupa: "" };
  data.ime = document.getElementById("imeInput").value;
  data.prezime = document.getElementById("prezimeInput").value;
  data.index = document.getElementById("indexInput").value;
  data.grupa = document.getElementById("grupaInput").value;

  StudentAjax.dodajStudenta(data, (err, dta) => {
    izvjestajUnosaElement = document.getElementById("ajaxstudent");
    if (!err) {
      izvjestajUnosaElement.innerHTML = dta;
    } else {
      izvjestajUnosaElement.innerHTML = err;
    }
  });
}

function postaviGrupu() {
  index = document.getElementById("indexInput").value;
  grupa = document.getElementById("grupaInput").value;

  StudentAjax.postaviGrupu(index, grupa, (err, dta) => {
    izvjestajUnosaElement = document.getElementById("ajaxstudent");
    if (!err) {
      izvjestajUnosaElement.innerHTML = dta;
    } else {
      izvjestajUnosaElement.innerHTML = err;
    }
  });
}

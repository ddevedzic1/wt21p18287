var StudentAjax = (function () {
  var dodajStudenta = function (student, fnCallback) {
    xhttp = new XMLHttpRequest();
    xhttp.addEventListener("readystatechange", function () {
      if (this.readyState === 4) {
        if (this.status === 200) {
          fnCallback(null, JSON.parse(this.responseText).status);
        } else if (this.status === 400) {
          fnCallback(JSON.parse(this.responseText).status, null);
        }
      }
    });
    xhttp.open("POST", "http://localhost:3000/student", true);
    xhttp.setRequestHeader("Content-Type", "application/json");
    xhttp.send(JSON.stringify(student));
  };

  var postaviGrupu = function (index, grupa, fnCallback) {
    xhttp = new XMLHttpRequest();
    xhttp.addEventListener("readystatechange", function () {
      if (this.readyState === 4) {
        if (this.status === 200) {
          fnCallback(null, JSON.parse(this.responseText).status);
        } else if (this.status === 400) {
          if (JSON.parse(this.responseText).status !== undefined)
            fnCallback(JSON.parse(this.responseText).status, null);
          else fnCallback(this.responseText, null);
        }
      }
    });
    xhttp.open("PUT", "http://localhost:3000/student/" + index, true);
    xhttp.setRequestHeader("Content-Type", "application/json");
    xhttp.send(JSON.stringify({ grupa: grupa }));
  };
  return {
    dodajStudenta: dodajStudenta,
    postaviGrupu: postaviGrupu,
  };
})();

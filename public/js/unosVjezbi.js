function dodajPolja() {
  brojVjezbi = Number.parseInt(
    document.getElementById("brojVjezbiInput").value
  );
  unosZadatakaDiv = document.getElementById("unosZadataka");
  VjezbeAjax.dodajInputPolja(unosZadatakaDiv, brojVjezbi);
}

function posalji() {
  let data = { brojVjezbi: 0, brojZadataka: [] };
  data.brojVjezbi = Number.parseInt(
    document.getElementById("brojVjezbiInput").value
  );
  let inputs = document.forms["forma"].getElementsByTagName("input");
  for (let i = 1; i < inputs.length; i++) {
    data.brojZadataka.push(Number.parseInt(inputs[i].value));
  }
  VjezbeAjax.posaljiPodatke(data, (err, dta) => {
    izvjestajUnosaElement = document.getElementById("izvjestajUnosa");
    if (!err) {
      izvjestajUnosaElement.innerHTML =
        "Uspješno ste unijeli podatke o vježbama";
    } else {
      izvjestajUnosaElement.innerHTML = err;
    }
  });
}

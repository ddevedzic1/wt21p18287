var VjezbeAjax = (function () {
  var dodajInputPolja = function (DOMelementDIVauFormi, brojVjezbi) {
    if (!isDOMElement(DOMelementDIVauFormi)) return;
    if (brojVjezbi > 0) {
      DOMelementDIVauFormi.innerHTML = "";
    }
    for (let i = 0; i < brojVjezbi; i++) {
      let label = document.createElement("label");
      label.innerHTML = "Zadatak" + (i + 1);
      let input = document.createElement("input");
      input.id = "z" + i;
      input.name = "z" + i;
      input.type = "number";
      input.value = 4;
      DOMelementDIVauFormi.appendChild(label);
      DOMelementDIVauFormi.appendChild(input);
    }
  };

  var posaljiPodatke = function (vjezbeObjekat, callbackFja) {
    xhttp = new XMLHttpRequest();

    xhttp.addEventListener("readystatechange", function () {
      if (this.readyState === 4) {
        if (this.status === 200) {
          callbackFja(null, this.responseText);
        } else if (this.status === 422 || this.status === 400) {
          callbackFja(JSON.parse(this.responseText).data, null);
        }
      }
    });

    xhttp.open("POST", "http://localhost:3000/vjezbe", true);
    xhttp.setRequestHeader("Content-Type", "application/json");
    xhttp.send(JSON.stringify(vjezbeObjekat));
  };

  var dohvatiPodatke = function (callbackFja) {
    var xhttp = new XMLHttpRequest();
    xhttp.addEventListener("readystatechange", function () {
      if (this.readyState === 4) {
        if (this.responseText.length === 0) {
          callbackFja("Greska: podaci nisu dostupni", null);
        } else callbackFja(null, JSON.parse(this.responseText));
      } else {
        callbackFja("Greska: podaci nisu dostupni", null);
      }
    });
    xhttp.open("GET", "http://localhost:3000/vjezbe");
    xhttp.send();
  };

  var iscrtajVjezbe = function (divDOMelement, vjezbeObjekat) {
    var brojVjezbi = vjezbeObjekat.nazivi.length;
    var brojZadataka = [];
    for (let i = 0; i < brojVjezbi; i++)
      brojZadataka.push(vjezbeObjekat.nazivi[i].naziviZadataka.length);
    if (!isDOMElement(divDOMelement)) return;
    if (
      typeof brojVjezbi === "undefined" ||
      typeof brojZadataka === "undefined"
    ) {
      divDOMelement.innerHTML = "Greska";
      return;
    } else {
      if (
        !(Number.isInteger(brojVjezbi) && brojVjezbi > 0 && brojVjezbi < 15)
      ) {
        divDOMelement.innerHTML = "Greska";
        return;
      } else {
        if (
          !(Object.prototype.toString.call(brojZadataka) === "[object Array]")
        ) {
          divDOMelement.innerHTML = "Greska";
          return;
        } else {
          if (brojVjezbi !== brojZadataka.length) {
            divDOMelement.innerHTML = "Greska";
            return;
          }

          for (let i = 0; i < brojZadataka.length; i++)
            if (
              !(
                Number.isInteger(brojZadataka[i]) &&
                brojZadataka[i] > -1 &&
                brojZadataka[i] < 10
              )
            ) {
              divDOMelement.innerHTML = "Greska";
              return;
            }
          divDOMelement.innerHTML = "";
          for (let i = 0; i < brojVjezbi; i++) {
            let div = document.createElement("div");
            div.innerHTML = vjezbeObjekat.nazivi[i].nazivVjezbe;
            div.onclick = () => {
              iscrtajZadatke(div, vjezbeObjekat.nazivi[i].naziviZadataka);
            };
            divDOMelement.appendChild(div);
          }
        }
      }
    }
  };

  var sakrijiZadatke = function (listaReferenciNaVjezbe) {
    for (let i = 0; i < listaReferenciNaVjezbe.length; i++)
      if (
        listaReferenciNaVjezbe.item(i).children.length === 1 &&
        listaReferenciNaVjezbe.item(i).children.item(0).style.display !== "none"
      )
        listaReferenciNaVjezbe.item(i).children.item(0).style.display = "none";
  };

  var iscrtajZadatke = function (vjezbaDOMelement, naziviZadataka) {
    if (!isDOMElement(vjezbaDOMelement)) return;
    if (vjezbaDOMelement.children.length === 0) {
      sakrijiZadatke(vjezbaDOMelement.parentElement.children);
      wrapperElement = document.createElement("div");
      for (let i = 0; i < naziviZadataka.length; i++) {
        let btn = document.createElement("button");
        btn.innerHTML = naziviZadataka[i];
        wrapperElement.appendChild(btn);
      }
      vjezbaDOMelement.appendChild(wrapperElement);
    } else {
      if (vjezbaDOMelement.children.item(0).style.display === "none") {
        sakrijiZadatke(vjezbaDOMelement.parentElement.children);
        vjezbaDOMelement.children.item(0).style.display = "block";
      } else vjezbaDOMelement.children.item(0).style.display = "none";
    }
  };
  var isDOMElement = function (o) {
    return typeof HTMLElement === "object"
      ? o instanceof HTMLElement
      : o &&
          typeof o === "object" &&
          o !== null &&
          o.nodeType === 1 &&
          typeof o.nodeName === "string";
  };
  return {
    dodajInputPolja: dodajInputPolja,
    posaljiPodatke: posaljiPodatke,
    dohvatiPodatke: dohvatiPodatke,
    iscrtajVjezbe: iscrtajVjezbe,
    iscrtajZadatke: iscrtajZadatke,
  };
})();

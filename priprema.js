const db = require("./db.js");
db.sequelize.sync({ force: true }).then(function () {
  inicializacija().then(function () {
    console.log("Gotovo kreiranje tabela i ubacivanje pocetnih podataka!");
    process.exit();
  });
});
function inicializacija() {
  var grupeListaPromisea = [];
  var studentiListaPromisea = [];
  var vjezbeListaPromisea = [];
  var zadaciListaPromisea = [];

  return new Promise(function (resolve, reject) {
    for (let i = 0; i < 25; i++) {
      zadaciListaPromisea.push(
        db.zadatak
          .create({
            naziv: "Zadatak" + (i + 1),
          })
          .then(function (k) {
            return new Promise(function (resolve, reject) {
              resolve(k);
            });
          })
      );
    }
    Promise.all(zadaciListaPromisea)
      .then(function (zadaci) {
        for (let i = 0; i < 5; i++) {
          vjezbeListaPromisea.push(
            db.vjezba.create({ naziv: "Vjezba" + (i + 1) }).then(function (b) {
              return b
                .setZadaciVjezbe(zadaci.slice(i * 5, (i + 1) * 5))
                .then(function () {
                  return new Promise(function (resolve, reject) {
                    resolve(b);
                  });
                });
            })
          );
        }
        Promise.all(vjezbeListaPromisea)
          .then(function (b) {
            resolve(b);
          })
          .catch(function (err) {
            console.log("Vjezbe greska " + err);
          });
      })
      .catch(function (err) {
        console.log("Zadaci greska " + err);
      });

    for (let i = 0; i < 25; i++) {
      studentiListaPromisea.push(
        db.student
          .create({
            ime: "ImeStudenta" + (i + 1),
            prezime: "PrezimeStudenta" + (i + 1),
            index: "" + (1000 * (i + 1) + 100 * (i + 1) + 10 * (i + 1) + i + 1),
          })
          .then(function (k) {
            return new Promise(function (resolve, reject) {
              resolve(k);
            });
          })
      );
    }
    Promise.all(studentiListaPromisea)
      .then(function (studenti) {
        for (let i = 0; i < 5; i++) {
          grupeListaPromisea.push(
            db.grupa.create({ naziv: "Grupa" + (i + 1) }).then(function (b) {
              return b
                .setStudentiGrupe(studenti.slice(i * 5, (i + 1) * 5))
                .then(function () {
                  return new Promise(function (resolve, reject) {
                    resolve(b);
                  });
                });
            })
          );
        }
        Promise.all(grupeListaPromisea)
          .then(function (b) {
            resolve(b);
          })
          .catch(function (err) {
            console.log("Grupe greska " + err);
          });
      })
      .catch(function (err) {
        console.log("Studenti greska " + err);
      });
  });
}
